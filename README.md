# Final Project

# Kelompok 18

# Anggota Kelompok

- Dzakwan Taqiyyuddin Al Fatah
- Riski Fitrianingsih
- Shinta Sri Wulandari

# Tema Project

Website membaca dan membuat sekumpulan cerita.

# ERD
![ERD](/uploads/769b7fc7726b30eb864b7b10d6321cca/ERD.jpg)

## Link Video dan Link Heroku

- Link demo Video [Kelompok 18 (this is Video)](https://drive.google.com/drive/folders/1SfOw52NeE_EGeZZKaZGNc2D85wY90Fq5?usp=sharing).
- Link Heroku https://readms.herokuapp.com/
