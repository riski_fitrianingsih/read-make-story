<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Cerita;

class Cerita extends Model
{
    protected $table = 'cerita';
    protected $fillable = [
        'judul',
        'user_id',
        'content',
        'poster',
        'genre_id'
    ];

        public function komentar()
    {
        return $this->hasMany(Komentar::class)->whereNull('parent_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->belongsTo(Like::class);
    }

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
}
