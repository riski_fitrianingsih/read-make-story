<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';
    protected $fillable = [
        'name'
    ];

    public function komentar()
{
    return $this->hasMany(Komentar::class)->whereNull('parent_id');
}
    public function cerita()
    {
    return $this->hasMany(Cerita::class);
    }
}
