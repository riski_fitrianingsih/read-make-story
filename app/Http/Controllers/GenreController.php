<?php

namespace App\Http\Controllers;

use Auth;
use Alert;
use DB;
use App\Genre;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class GenreController extends Controller
{

    public function create() {
     return view ('genre.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required|unique:genre|max:20',
        ]);
        $query = DB::table('genre')->insert([
            "name" => $request["name"], 
        ]);
        alert()->success('Berhasil','Data Genre ditambahkan!');
        return redirect('/genre'); 
    }

    public function index() {
        $genre = DB::table('genre')->get();
        // dd($genre);
        return view ('genre.index' , compact('genre'));
       }
    
       public function edit($id) {
        $genre = DB::table('genre')->where('id', $id)->first();
       
        return view ('genre.edit' , compact('genre'));
    }

    public function update($id , Request $request ) {
        $request->validate([
            'name' => 'required',
        ]);

        $query = DB::table('genre')
            ->where('id', $id)
            ->update([
            'name' => $request['name'],
            ]);
        alert()->success('Berhasil','Data Genre diubah!');
        return redirect('/genre');
    }

    public function destroy($id)
    {
        $query = DB::table('genre')->where('id', $id)->delete();
        alert()->success('Berhasil','Data Genre dihapus!');
        return redirect('/genre');
    }

}
// push baru dari heroku
