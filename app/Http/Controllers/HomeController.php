<?php

namespace App\Http\Controllers;

use App\User;
use App\Genre;
use App\Cerita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
Use Alert;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        toast('Login Berhasil', 'success')->position('center')->width('300px')->timerProgressBar()->background('#DAEAEC');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user_id = Auth::user()->id;
        $cerita = Cerita::where('user_id', '=', $user_id)->count();
        $genre = Genre::count();
        $user = User::where('role', 'user')->count();
        $total_genre = DB::table('cerita')
        ->where('user_id','=',$user_id)
        ->join('genre', 'cerita.genre_id', '=', 'genre.id')
        ->select(DB::raw('count(genre_id) as total, genre.name as genre'))
        ->groupBy('genre.name')
        ->get();

        return view('index', compact('cerita', 'genre', 'user','total_genre'));
    }
}
// push baru dari heroku
