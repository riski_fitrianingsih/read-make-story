<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $guarded = [];
    
    public function komen()
    {
        return $this->hasMany(Komentar::class, 'parent_id');
    }
}
