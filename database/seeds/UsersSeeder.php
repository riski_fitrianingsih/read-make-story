<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            'name' => 'Zulia',
            'email' => 'zulia@admin.com',
            'role' => 'admin',
            'image' => '/uploads/images/profile/default.png',
            'password' => Hash::make('password'),
            );
            User::create($data);
    }
}
