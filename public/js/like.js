var ceritaId = 0;

$(".like").on("click", function(event) {
    event.preventDefault();
    ceritaId = event.target.parentNode.parentNode.dataset["ceritaid"];
    var isLike = event.target.previousElementSibling == null;

    $.ajax({
        method: "POST",
        url: urlLike,
        data: { isLike: isLike, ceritaId: ceritaId, _token: token }
    }).done(function() {
        event.target.innerText = isLike ?
            event.target.innerText == "Like" ?
            "You Like this Story" :
            "Like" :
            event.target.innerText == "Dislike" ?
            "You dont Like this Story" :
            "Dislike";
        if (isLike) {
            event.target.nextElementSibling.innerText = "Dislike";
        } else {
            event.target.previousElementSibling.innerText = "Like";
        }
    });
});