

@extends ('layouts.master')
@section('title', 'Cerita')
@section('content')
<div class="content">
</div>

<div class="section-header">
    <h1>Cerita</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item">Cerita</div>
    </div>
  </div>

    <section class="content" style="padding-top: 5px">
      <div style="margin-bottom: 10px;" class="row">
    <div class="col-lg-12">
          
            @if (session('status'))
                <div class="">
                    {{ session('status') }}
                </div>
            @endif
    </div>
      </div>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h4>Cerita</h4>
        </div>

        
        
        {{-- MODAL EDIT DATA --}}
        
                    <form action="/cerita/{{$cerita->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul" class="text">Judul</label>
                            <input id="judul" name="judul" value="{{$cerita->judul}}" placeholder="Masukkan Judul" type="text" class="form-control">
                            @error('judul')
                                <div class="alert alert-danger">
                                {{$message}}
                                </div>
                            @enderror
                        </div>
                        </div>

                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="genre_id" class="text">Genre Cerita</label>
                            <select name="genre_id" id="genre_id" class="form-control">
                            <option value=""> ---Pilih Genre-- </option>
                            @foreach ($genre as $item)
                            @if ($item->id === $cerita->genre_id)
                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                            @else
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endif
                                
                            @endforeach
                            </select>
                            @error('genre_id')
                            <div class="alert alert-danger">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                        </div>



                        <div class="col-md-12">
                        <div class="form-group">
                            <label for="content" class="text">Content</label>
                            <textarea id="content" name="content" placeholder="Masukkan Content" type="text" class="form-control">{{$cerita->content}}</textarea>
                            @error('content')
                            <div class="alert alert-danger">
                            {{$message}}
                            </div>
                        @enderror
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="poster" class="text">Poster</label>
                            <img width="200" height="200"/>
                            <input type="file" id="poster" name="poster" class="form-control border-primary uploads" style="margin-top: 20px;">
                            @error('poster')
                            <div class="alert alert-danger">
                            {{$message}}
                            </div>
                        @enderror
                        </div>
                        </div>
                </div>
                </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
               
        
        
      </div>
    </div>
  </div>
    </section>


@endsection
@push ('page-scripts')
@include('cerita.js.cerita-js')
@endpush