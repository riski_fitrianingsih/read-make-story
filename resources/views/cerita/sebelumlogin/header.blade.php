<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title') | RMS</title>
  <link rel="icon" href="img/Fiticon.png" type="image/png">

  <!-- Bootstrap core CSS -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/css/small-business.css')}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="indexafterlogin.php">Read Make Story</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Read</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('cerita') }}">Make</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Genre
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              <a class="dropdown-item" href="#Horror">Horror</a>
              <a class="dropdown-item" href="#Fantasy">Fantasy</a>
              <a class="dropdown-item" href="#Romance">Romance</a>
              <a class="dropdown-item" href="#Drama">Drama</a>
            </div>
          </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @if (Auth::user() )
            {{ Auth::user()->name }}
            @else
                
            Masuk
            @endif
          </a>
            @guest
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
            <a class="dropdown-item" href="{{ route('login') }}">{{ __('Login') }}</a>
            @if (Route::has('register'))
            <a class="dropdown-item" href="{{ route('register') }}">{{ __('Daftar') }}</a>
            @endif
            @else
            {{-- {{ Auth::user()->name }} --}}
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item has-icon text-danger">{{ __('Logout') }}
              <i class="fas fa-sign-out-alt"></i></a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
          </div>
          @endguest

        </li>
        </ul>
      </div>
    </div>
  </nav>