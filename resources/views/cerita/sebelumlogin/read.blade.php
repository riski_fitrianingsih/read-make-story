<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Fitria Blog - Home</title>
  <link rel="icon" href="img/Fiticon.png" type="image/png">

  <!-- Bootstrap core CSS -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('template/startbootstrap-small-business-gh-pages/css/small-business.css')}}" rel="stylesheet">
  <link href="{{asset('css/modern-business.css')}}" rel="stylesheet">
  

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Read Make Story</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="read.php">Read</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php">Make</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Genre
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
              <a class="dropdown-item" href="#Horror">Horror</a>
              <a class="dropdown-item" href="#Fantasy">Fantasy</a>
              <a class="dropdown-item" href="#Romance">Romance</a>
              <a class="dropdown-item" href="#Drama">Drama</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  
 <!-- /.navbar -->


<!-- Page Content -->
<header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('img/rms1.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms2.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('img/rms3.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3>Third Slide</h3>
            <p>This is a description for the third slide.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>

<!-- Page Content -->
<!-- Page Content 1 -->
<center><h1 class="my-4"><a name="Horror">Horror</a></h1></center>
<div class="container">

  <!-- Heading Row -->

  <?php $baris=mysqli_fetch_array($hasil)  ?>

  <div class="row align-items-center my-5">
    <div class="col-lg-7">
      <img class="img-fluid rounded mb-4 mb-lg-0" src="<?php echo $baris[5]; ?>" alt="">
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-5">
      <h1 class="font-weight-light">Horror - <?php echo $baris['title']; ?></h1>
      <p><?php echo $baris[3]; ?></p>
      <a class="btn btn-primary" href='readpost.php?id_horror=<?php echo $baris[0] ?>'>Read!</a>
    </div>
    <!-- /.col-md-4 -->
  </div>
  <!-- /.row -->

  <!-- Call to Action Well -->
  <div class="card text-white bg-secondary my-5 py-4 text-center">
    <div class="card-body">
      <p class="text-white m-0">The more that you read, the more things you will know. – Dr. Seuss</p>
    </div>
  </div>

  <!-- Content Row -->
  
  <div class="row">
    <?php while ($baris=mysqli_fetch_array($hasil)) { ?>
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title"><?php echo $baris['title']; ?></h2>
          <p class="card-text"><?php echo $baris['kilas']; ?></p>
        </div>
        <div class="card-footer">
          <a href='readpost.php?id_horror=<?php echo $baris['id_horror'] ?>' class="btn btn-primary btn-sm">Read!</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  
  <!-- /.row -->

</div>

<!-- /.container -->

<!-- Page Content 2 -->
<center><h1 class="my-4"><a name="Fantasy">Fantasy</a></h1></center>
<div class="container">

  <!-- Heading Row -->

  <?php $baris1=mysqli_fetch_array($hasil1)  ?>

  <div class="row align-items-center my-5">
    <div class="col-lg-7">
      <img class="img-fluid rounded mb-4 mb-lg-0" src="<?php echo $baris1[5]; ?>" alt="">
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-5">
      <h1 class="font-weight-light">Fantasy - <?php echo $baris1['title']; ?></h1>
      <p><?php echo $baris1[3]; ?></p>
      <a class="btn btn-primary" href='readpost1.php?id_fantasy=<?php echo $baris1[0] ?>'>Read!</a>
    </div>
    <!-- /.col-md-4 -->
  </div>
  <!-- /.row -->

  <!-- Call to Action Well -->
  <div class="card text-white bg-secondary my-5 py-4 text-center">
    <div class="card-body">
      <p class="text-white m-0">The more that you read, the more things you will know. – Dr. Seuss</p>
    </div>
  </div>

  <!-- Content Row -->
  
  <div class="row">
    <?php while ($baris1=mysqli_fetch_array($hasil1)) { ?>
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title"><?php echo $baris1['title']; ?></h2>
          <p class="card-text"><?php echo $baris1['kilas']; ?></p>
        </div>
        <div class="card-footer">
          <a href='readpost1.php?id_fantasy=<?php echo $baris1['id_fantasy'] ?>' class="btn btn-primary btn-sm">Read!</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  
  <!-- /.row -->

</div>

<!-- /.container -->

<!-- Page Content 3 -->
<center><h1 class="my-4"><a name="Romance">Romance</a></h1></center>
<div class="container">

  <!-- Heading Row -->

  <?php $baris2=mysqli_fetch_array($hasil2)  ?>

  <div class="row align-items-center my-5">
    <div class="col-lg-7">
      <img class="img-fluid rounded mb-4 mb-lg-0" src="<?php echo $baris2[5]; ?>" alt="">
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-5">
      <h1 class="font-weight-light">Romance - <?php echo $baris2['title']; ?></h1>
      <p><?php echo $baris2[3]; ?></p>
      <a class="btn btn-primary" href='readpost2.php?id_romance=<?php echo $baris2[0] ?>'>Read!</a>
    </div>
    <!-- /.col-md-4 -->
  </div>
  <!-- /.row -->

  <!-- Call to Action Well -->
  <div class="card text-white bg-secondary my-5 py-4 text-center">
    <div class="card-body">
      <p class="text-white m-0">The more that you read, the more things you will know. – Dr. Seuss</p>
    </div>
  </div>

  <!-- Content Row -->
  
  <div class="row">
    <?php while ($baris2=mysqli_fetch_array($hasil2)) { ?>
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title"><?php echo $baris2['title']; ?></h2>
          <p class="card-text"><?php echo $baris2['kilas']; ?></p>
        </div>
        <div class="card-footer">
          <a href='readpost2.php?id_romance=<?php echo $baris2['id_romance'] ?>' class="btn btn-primary btn-sm">Read!</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  
  <!-- /.row -->

</div>

<!-- /.container -->

<!-- Page Content 4 -->
<center><h1 class="my-4"><a name="Drama">Drama</a></h1></center>
<div class="container">

  <!-- Heading Row -->

  <?php $baris3=mysqli_fetch_array($hasil3)  ?>

  <div class="row align-items-center my-5">
    <div class="col-lg-7">
      <img class="img-fluid rounded mb-4 mb-lg-0" src="<?php echo $baris3[5]; ?>" alt="">
    </div>
    <!-- /.col-lg-8 -->
    <div class="col-lg-5">
      <h1 class="font-weight-light">Drama - <?php echo $baris3['title']; ?></h1>
      <p><?php echo $baris3[3]; ?></p>
      <a class="btn btn-primary" href='readpost3.php?id_drama=<?php echo $baris3[0] ?>'>Read!</a>
    </div>
    <!-- /.col-md-4 -->
  </div>
  <!-- /.row -->

  <!-- Call to Action Well -->
  <div class="card text-white bg-secondary my-5 py-4 text-center">
    <div class="card-body">
      <p class="text-white m-0">The more that you read, the more things you will know. – Dr. Seuss</p>
    </div>
  </div>

  <!-- Content Row -->
  
  <div class="row">
    <?php while ($baris3=mysqli_fetch_array($hasil3)) { ?>
    <div class="col-md-4 mb-5">
      <div class="card h-100">
        <div class="card-body">
          <h2 class="card-title"><?php echo $baris3['title']; ?></h2>
          <p class="card-text"><?php echo $baris3['kilas']; ?></p>
        </div>
        <div class="card-footer">
          <a href='readpost3.php?id_drama=<?php echo $baris3['id_drama'] ?>' class="btn btn-primary btn-sm">Read!</a>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  
  <!-- /.row -->

</div>

<!-- /.container -->


<!-- Footer -->
  
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Fitria Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>

<!-- /.footer -->