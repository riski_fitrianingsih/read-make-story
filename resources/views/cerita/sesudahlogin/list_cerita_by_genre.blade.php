@extends('cerita.sesudahlogin.master')
@section('title', 'List Cerita')
@section('content')


    <!-- Page Content -->
    <div class="container">

        <h1 class="my-4">Read Make Story</h1>
    
        <!-- Marketing Icons Section -->
        <center>
          <div class="row">
          @foreach ($cerita as $item)
          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <h4 class="card-header">{{ $item->judul }}</h4>
              <div class="card-body">
                <p class="card-text"><img src="../../{{ $item->poster }}" width="200"></p>
                <p class="card-text">{{ $item->genre->name }}</p>
                <blockquote class="blockquote mb-0">
                    <p class="text-justify-10">{{ substr($item->content, 0, 20) }}</p>
                    <footer class="blockquote-footer">Posted on {{ ($item->created_at)->format('d F Y') }} by <cite title="Source Title">{{ $item->user->name }}</cite></footer>
                  </blockquote>
                  <div class="cerita" data-ceritaid="{{ $item->id }}">
                  @if (Auth::user())
                  <a href="#" class="like">{{ Auth::user()->likes()->where('cerita_id', $item->id)->first() ? Auth::user()->likes()->where('cerita_id', $item->id)->first()->like == 1 ? 'You Like this Story' : 'Like' : 'Like'}}</a>
                  <a href="#" class="like">{{ Auth::user()->likes()->where('cerita_id', $item->id)->first() ? Auth::user()->likes()->where('cerita_id', $item->id)->first()->like == 0 ? 'You dont Like this Story' : 'Dislike' : 'Dislike'}}</a>
                  @endif
                  
                </div>
                  </div>
              <div class="card-footer">
                <a href="{{ route('show.cerita', $item->id )}}" class="btn btn-primary">Mulai Membaca Cerita</a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </center>
    </div>
    
@endsection
@push('script')
<script src="{{ asset('/js/like.js') }}"></script>
    <script type="text/javascript">
        var token = '{{ Session::token() }}';
        var urlLike = '{{ route('like') }}';
    </script>
@endpush
{{-- push baru dari heroku --}}
