 <!-- Navbar -->
 @include('cerita.sesudahlogin.header')
 <!-- /.navbar -->


<!-- Page Content -->
@yield('content')


<!-- Footer -->
@include('cerita.sesudahlogin.footer')
<!-- /.footer -->
@stack('script')
