<div class="row">
    <div class="card-body">
        <ul class="list-group">
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Judul</h6>
            <p class="mb-0">{{ $cerita->judul }}</p>
          </li>
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Konten</h6>
            <p class="mb-0">{{ $cerita->content }}</p>
          </li>
          <li class="list-group-item">
            <h6 class="mb-0 mt-1">Poster</h6>
            <p class="mb-0">{{ $cerita->poster }}</p>
          </li>
        </ul>
      </div>