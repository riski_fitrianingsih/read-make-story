@extends ('layouts.master')
@section('title', 'Genre')
@section('content')
<div class="section-header">
    <h1>Genre</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item">Genre</div>
    </div>
  </div>
  <section class="content" style="padding-top: 5px">
    <div style="margin-bottom: 10px;" class="row">
</div>

<form role="post" action="/genre" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Tambah Nama Genre</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Kategori Genre">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
    <a type="submit" class="btn btn-primary" href="/genre">Tampilan List Genre</a>
</form>

@endsection
@push ('page-scripts')
@include('cerita.js.cerita-js')
@endpush
