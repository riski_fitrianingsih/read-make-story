@extends ('layouts.master')
@section('title', 'Genre')
@section('content')
<div class="section-header">
    <h1>EDIT Kategori Genre</h1>
  </div>
  <section class="content" style="padding-top: 5px">
    <div style="margin-bottom: 10px;" class="row">
</div>

<form role="post" action="/genre/{{$genre -> id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">EDIT Nama Genre {{$genre -> id}}</label>
        <input type="text" class="form-control" name="name" id="name" value="{{old ('name' , $genre->name)}}" placeholder="Edit Jenis Genre">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
    <a type="submit" class="btn btn-primary" href="/genre">Tampilan List Genre</a>
</form>

@endsection
@push ('page-scripts')
@include('cerita.js.cerita-js')
@endpush
