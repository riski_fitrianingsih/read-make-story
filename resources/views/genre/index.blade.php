@extends ('layouts.master')
@section('title', 'Genre')
@section('judul')
   List Table Genre
@endsection
@section('content')

<div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header"> 
            <h3 class="card-title"> List Table Genre</h3>
    </div>
    <a type="submit" class="btn btn-primary" href="/genre/create">Create New Genre</a>
</div>
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Nama Genre</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($genre as $key => $genres)
        <tr>
            <td> {{$key + 1}} </td>
            <td> {{$genres -> name}} </td>
            <td style="display: flex;">
              <a type="submit" class="btn btn-primary my-2" href="/genre/{{$genres->id}}/edit">Edit</a>
              <form action="/genre/{{$genres->id}}" method="POST"> 
              @csrf
              @method('delete')
              <input type="submit" value="delete" class="btn btn-danger my-2 py-2" >
              </form>


            </td>
        </tr>  
      @endforeach
      
        
    </tbody>
  </table>



@endsection