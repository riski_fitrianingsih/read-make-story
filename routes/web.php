<?php

use App\Genre;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//test
Auth::routes();

// Route::get('/', function () {
//     return redirect(route('login'));
// });

Route::get('/', function () {
  $genre = Genre::all();
  return view('cerita.sebelumlogin.index',  compact('genre'));
});
Route::get('/list-cerita', 'CeritaController@list_cerita')->name('list.cerita');
Route::get('/list-cerita/genre/{genre}', 'CeritaController@by_genre')->name('list.cerita.genre');


Route::group(['middleware' => 'auth'], function() {

    // CERITA
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/cerita', 'CeritaController@index')->name('cerita');
    Route::post('/cerita', 'CeritaController@store')->name('store');
    Route::get('/cerita/{cerita}/edit', 'CeritaController@edit')->name('edit');
    Route::patch('/cerita/update/{cerita}', 'CeritaController@update')->name('update');
    Route::get('/cerita/show/{cerita}', 'CeritaController@show')->name('show');
    Route::delete('/cerita/{cerita}', 'CeritaController@destroy')->name('cerita.delete');
    Route::get('/show/cerita/{cerita}', 'CeritaController@show_cerita')->name('show.cerita');
    Route::post('/like', 'CeritaController@like')->name('like');

    // GENRE
    Route::get('/genre', 'GenreController@index')->name('genre');
    Route::get('/genre/create', 'GenreController@create')->name('genre.create');
    Route::post('/genre', 'GenreController@store')->name('genre.store');
    Route::get('/genre/{id}/edit', 'GenreController@edit')->name('edit');
    Route::put('/genre/{id}', 'GenreController@update')->name('update');
    Route::delete('/genre/{id}', 'GenreController@destroy')->name('genre.delete');

    
    
  // Profile
    Route::get('profile', 'Admin\UserController@profile')->name('profile');
    Route::post('profile', 'Admin\UserController@update')->name('updateProfile');
    Route::post('profile/image', 'Admin\UserController@ubahfoto')->name('ubahFoto');
    Route::patch('profile/password', 'Admin\UserController@ubahPassword')->name('ubahPassword');

  // Manajemen User
  Route::get('/manajemen_user', 'UserController@index')->name('manajemen_user.index');
  Route::post('/manajemen_user', 'UserController@store')->name('manajemen_user.store');
  Route::get('/manajemen_user/{manajemen_user}/edit', 'UserController@edit')->name('edit');
  Route::patch('/manajemen_user/update/{manajemen_user}', 'UserController@update')->name('update');
  Route::get('/manajemen_user/show/{manajemen_user}', 'UserController@show')->name('show');
  Route::delete('/manajemen_user/{manajemen_user}', 'UserController@destroy')->name('manajemen_user.delete');

  // Komentar
  Route::post('/komentar', 'KomentarController@store');
});



